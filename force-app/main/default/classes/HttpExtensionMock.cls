@IsTest
global class HttpExtensionMock implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        if(request.getEndpoint().contains('token')) {
            response.setBody('{"access_token": "ya-Iuhfjkkyuojjfjh", "expires_in": 3599}');
        } else {
            response.setBody('{}');
        }
        return response;
    }
}