/**
 * HttpMultipart : is useful to send binary data from Apex.
 * For more information https://formations.djammadev.com/sf-multipart.html
 * @author : Cheick Sissoko, Djamma Dev
 * @since  : 2022-05-02
 */
global with sharing class HttpMultipart {
    private static final String MULTIPART_FORM_DATA = 'multipart/form-data';
    private static final String CONTENT_TYPE = 'Content-Type';
    private static final String POST = 'POST';
    private static final String RN = '\r\n';
    /**
     * Concat to base64 encoded string
     * @return concatenated string
     */
    global static String concat(String left, String right) {
        Blob bytesLeft = EncodingUtil.base64Decode(left);
        Blob bytesRight = EncodingUtil.base64Decode(right);
        String combined = EncodingUtil.convertToHex(bytesLeft) + EncodingUtil.convertToHex(bytesRight);
        return EncodingUtil.base64Encode(EncodingUtil.convertFromHex(combined));
    }

    /**
     * Create end datagram
     */
    global static String createDatagram(String boundary) {
        return EncodingUtil.base64Encode(Blob.valueOf('--' + boundary + '--'));
    }

    /**
     * @see createBase64Datagram()
     */
    global static String createDatagram(String boundary, String field, String message) {
        return createBase64Datagram(boundary, field, EncodingUtil.base64Encode(Blob.valueOf(message)), null);
    }

    /**
     * Create a part of multipart body
     * @param boundary part separator
     * @param field part name
     * @param encodedMessage encoded message
     * @param filename if the message is a file body
     */
    global static String createBase64Datagram(String boundary, String field, String encodedMessage, String filename) {
        String header = 'Content-Disposition: form-data; name="' + field + '"';
        if(String.isNotBlank(filename)) {
            header += '; filename="' + filename + '"';
        }
        String frn = '';
        if (encodedMessage.endsWith('==')) {
            encodedMessage = encodedMessage.substring(0, encodedMessage.length() - 2) + '0K';
        } else if (encodedMessage.endsWith('=')) {
            encodedMessage = encodedMessage.substring(0, encodedMessage.length() - 1) + 'N';
            frn = '\n';
        } else {
            frn = RN;
        }
        String headerPart = '--' +
                            boundary +
                            RN +
                            header;
        String encodedHeader = EncodingUtil.base64Encode(Blob.valueOf(headerPart));
        while (encodedHeader.endsWith('=')) {
            headerPart += ' ';
            encodedHeader = EncodingUtil.base64Encode(Blob.valueOf(headerPart));
        }
        String part1 = headerPart +
                RN +
                RN;
        String temp = concat(EncodingUtil.base64Encode(Blob.valueOf(part1)), encodedMessage);
        String lastPart = frn;
        return concat(temp, EncodingUtil.base64Encode(Blob.valueOf(lastPart)));
    }

    global class Callout implements Queueable, Database.AllowsCallouts {
        private final String endpoint;
        private final Map<String, String> messages;
        private final Map<String, FilePart> attachments;
        private final Map<String, String> headers = new Map<String,String>();
        private HttpListener listener;
        global Callout(String endpoint, Map<String, String> messages, Map<String, FilePart> attachments) {
            this.endpoint = endpoint;
            this.messages = messages;
            this.attachments = attachments;
        }

        private String buildBody(String boundary) {
            String body = null;
            for(String field : messages.keySet()) {
                String message = messages.get(field);
                String encoded = createDatagram(boundary, field, message);
                if(body == null) {
                    body = encoded;
                } else {
                    body = concat(body, encoded);
                }
            }
            for(String field : attachments.keySet()) {
                FilePart part = attachments.get(field);
                String encoded = createBase64Datagram(boundary, field, part.body, part.filename);
                if(body == null) {
                    body = encoded;
                } else {
                    body = concat(body, encoded);
                }
            }
            String endData = createDatagram(boundary);
            if(body == null) {
                body = endData;
            } else {
                body = concat(body, endData);
            }
            return body;
        }

        global Callout addHeader(String key, String value) {
            headers.put(key, value);
            return this;
        }

        global Callout setListener(HttpListener listener) {
            this.listener = listener;
            return this;
        }

        global void execute(QueueableContext context) {
            String boundary = '-------------ddmutipart-------' + Integer.valueof((Math.random() * 1000)) + '-' + Integer.valueof((Math.random() * 1000));
            String body = buildBody(boundary);
            Blob bodyBlob = EncodingUtil.base64Decode(body);
            HttpExtension req = new HttpExtension();
            req.setMethod(POST);
            req.setEndpoint(endpoint);
            req.getRequest().setHeader(CONTENT_TYPE, MULTIPART_FORM_DATA+'; boundary='+boundary);
            req.getRequest().setTimeout(30000); // Wait for 30s before timeout.
            for(String key : headers.keySet()) {
                req.getRequest().setHeader(key, headers.get(key));
            }
            req.getRequest().setBodyAsBlob(bodyBlob);
            try {
                HTTPResponse res = req.send();
                if (listener != null) {
                    listener.onResponse(res);
                } else {
                    System.debug('HttpMultipart:Callout: ' + res.getBody());
                }
            } catch (Exception e) {
                System.debug('HttpMultipart:Callout: ' + e.getMessage());
                throw new HttpMultipartException(e.getMessage());
            }
        }
    }

    global class FilePart {
        String filename;
        String body;

        global FilePart(String filename, String body) {
            this.filename = filename;
            if(String.isNotBlank(body)) {
                if(body.startsWith('data')) {
                    Integer index = body.indexOf(',');
                    body = body.substring(index + 1);
                }
            }
            this.body = body;
        }
    }

    global interface HttpListener {
        void onResponse(HttpResponse response);
    }

    global class HttpMultipartException extends Exception {}
}