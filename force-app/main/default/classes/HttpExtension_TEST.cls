@IsTest
public class HttpExtension_TEST {

    @IsTest
    static void sendJWT() {
        Test.setMock(HttpCalloutMock.class, new HttpExtensionMock());
        HttpExtension extension = new HttpExtension();
        extension.setMethod('GET');
        extension.setEndPoint('jwt:named_credentials');
        HttpResponse response = extension.send();
        System.assertEquals(200, response.getStatusCode());
    }

    @IsTest
    static void sendClientCredentials() {
        Test.setMock(HttpCalloutMock.class, new HttpExtensionMock());
        HttpExtension extension = new HttpExtension();
        extension.setMethod('GET');
        extension.setEndPoint('client_credentials:named_credentials');
        HttpResponse response = extension.send();
        System.assertEquals(200, response.getStatusCode());
    }

    @IsTest
    static void extractSchema() {
        // Callout
        System.assertEquals('callout', HttpExtension.extractSchema('callout:NamedCredentials_Callout/path'));
        System.assertEquals('callout', HttpExtension.extractSchema('callout:NamedCredentials'));
        System.assertEquals(null, HttpExtension.extractSchema('callout:'));

        // Client_credentials
        System.assertEquals('client_credentials', HttpExtension.extractSchema('client_credentials:NamedCredentials_Callout/path'));
        System.assertEquals('client_credentials', HttpExtension.extractSchema('client_credentials:NamedCredentials'));
        System.assertEquals(null, HttpExtension.extractSchema('client_credentials:_34'));

        // JWT
        System.assertEquals('jwt', HttpExtension.extractSchema('jwt:NamedCredentials_Callout/path'));
        System.assertEquals('jwt', HttpExtension.extractSchema('jwt:NamedCredentials'));
        System.assertEquals(null, HttpExtension.extractSchema('jwt:4RTbfjk'));
    }

    @IsTest
    static void extractConfigName() {
        // Callout
        System.assertEquals('NamedCredentials_Callout', HttpExtension.extractConfigName('callout:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials', HttpExtension.extractConfigName('callout:NamedCredentials'));
        System.assertEquals(null, HttpExtension.extractConfigName('callout:'));

        // Client_credentials
        System.assertEquals('NamedCredentials_Callout', HttpExtension.extractConfigName('client_credentials:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials', HttpExtension.extractConfigName('client_credentials:NamedCredentials?parm=1'));
        System.assertEquals(null, HttpExtension.extractConfigName('client_credentials:_34'));

        // JWT
        System.assertEquals('NamedCredentials_Callout', HttpExtension.extractConfigName('jwt:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials', HttpExtension.extractConfigName('jwt:NamedCredentials'));
        System.assertEquals(null, HttpExtension.extractConfigName('jwt:4RTbfjk'));
    }

    @IsTest
    static void extractPath() {
        // Callout
        System.assertEquals('NamedCredentials_Callout/path', HttpExtension.extractPath('callout:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials', HttpExtension.extractPath('callout:NamedCredentials'));
        System.assertEquals('', HttpExtension.extractPath('callout:'));

        // Client_credentials
        System.assertEquals('NamedCredentials_Callout/path', HttpExtension.extractPath('client_credentials:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials?parm=1', HttpExtension.extractPath('client_credentials:NamedCredentials?parm=1'));
        System.assertEquals('', HttpExtension.extractPath('client_credentials:_34'));

        // JWT
        System.assertEquals('NamedCredentials_Callout/path', HttpExtension.extractPath('jwt:NamedCredentials_Callout/path'));
        System.assertEquals('NamedCredentials_Callout/path?test=123', HttpExtension.extractPath('jwt:NamedCredentials_Callout/path?test=123'));
        System.assertEquals('NamedCredentials', HttpExtension.extractPath('jwt:NamedCredentials'));
        System.assertEquals('', HttpExtension.extractPath('jwt:4RTbfjk'));
    }
}