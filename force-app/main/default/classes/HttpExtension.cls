/**
 * Http extension to perform JWT Exchange and client_credentials OAuth2
 * This class uses the CustomMetadata JWTExchange__mdt to retrieve the exchange information.
 * A NamedCredential with the same DeveloperName as JWTExchange__mdt should exist.
 * For more information https://formations.djammadev.com/http-ext.html
 *  
 * @author : Cheick Sissoko
 * @since  : 2021-09-02
 */
global class HttpExtension {
    private static final String HTTP_EXT_SCHEMA_REGEX = '(callout|jwt|client_credentials):(([A-Za-z][A-Za-z0-9_]+)(.*)?)';
	private String method;
    private String endPoint;
    public static final long TOKEN_VALID_FOR = 3600; // one hour.
    private static final String TEST_ENDPOINT = 'https://token-endpoint.com';
    private final HttpRequest request;
    
    global HttpExtension() {
        this.request = new HttpRequest();
    }
    
    global String getMethod() {
        return this.method;
    }
    
    global void setMethod(String method) {
        this.method = method;
    }
    
    global String getEndPoint() {
        return this.endPoint;
    }
    
    global void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }
    
    global HttpRequest getRequest() {
        return this.request;
    }
    
    global static JWTExchange__mdt getJWTConfig(String name) {
        List<JWTExchange__mdt> configs = [Select Id, Algorithm__c
                                          		, Issuer__c
                                          		, Subject__c
                                          		, Audience__c
                                          		, Scope__c
                                          		, TokenValidFor__c
                                          		, SigningKey__c
                                          		, TokenUrl__c 
                                          From JWTExchange__mdt 
                                          Where DeveloperName =: name];
        return configs.isEmpty() ? (Test.isRunningTest() ?  new JWTExchange__mdt(TokenUrl__c = TEST_ENDPOINT, Algorithm__c='HS256', SigningKey__c='MySuperKey') : new JWTExchange__mdt()) : configs[0];
    }

    global static OAuth2Parameter__mdt getClientCredentialsConfig(String name) {
        List<OAuth2Parameter__mdt> configs = [Select Id
                                          		, Audience__c
                                          		, Scope__c
                                          		, TokenUrl__c 
                                          From OAuth2Parameter__mdt 
                                          Where DeveloperName =: name];
        return configs.isEmpty() ? (Test.isRunningTest() ?  new OAuth2Parameter__mdt(TokenUrl__c = TEST_ENDPOINT) : new OAuth2Parameter__mdt()) : configs[0];
    }
    
    global static String base64URLencode(Blob input) { 
        String output = encodingUtil.base64Encode(input);
        output = output.replace('+', '-');
        output = output.replace('/', '_');
        while (output.endsWith('=')) {
            output = output.subString(0,output.length()-1);
        }
        return output;
    }
    
    global static String sign(String token, String alg, String signingKey) {
        String signature = '';
        if (alg == 'RS256') {
            Blob key = EncodingUtil.base64Decode(signingKey);
            signature = base64URLencode(Crypto.sign('rsa-sha256', Blob.valueof(token), key));
        } else if (alg == 'HS256') {
            Blob key = Blob.valueOf(signingKey);
            signature = base64URLencode(Crypto.generateMac('hmacSHA256', Blob.valueof(token), key));
        } else {
            throw new JWTExchangeException('Invalid algorithm. Authorized alg are: RS256 and HS256.');
        }
        return signature;
    }
    
    global static String getJWTToken(JWTExchange__mdt config) {
        Map<String, String> header = new Map<String, String>();
        header.put('typ', 'JWT');
        header.put('alg', config.Algorithm__c);
        String serializedHeader = JSON.serialize(header, true);
        String encodedHeader = base64URLencode(Blob.valueOf(serializedHeader));
        Map<String, Object> body = new Map<String, Object>();
        body.put('iss', config.Issuer__c);
        body.put('sub', config.Subject__c);
        body.put('aud', config.Audience__c);
        body.put('scope', config.Scope__c);
        long iat = (dateTime.now().getTime() / 1000);
        long exp = iat + (long) (config.TokenValidFor__c == null || config.TokenValidFor__c <= 0 ? TOKEN_VALID_FOR : config.TokenValidFor__c);
        body.put('iat', iat);
        body.put('exp', exp);
        String serializedBody = JSON.serialize(body, true);
        String encodedBody = base64URLencode(Blob.valueOf(serializedBody));
        String token = encodedHeader + '.' + encodedBody;
        String signingKey = config.SigningKey__c;
        if(String.isNotBlank(signingKey)) {
            signingKey = signingKey.replaceAll('-{3,}(BEGIN|END) +(RSA +)?PRIVATE KEY(-{3,})', '');
            signingKey = signingKey.replace('\n', '');
        }
        String signature = sign(token, config.Algorithm__c, signingKey);
        token = token + '.' + signature;
        return token;
    }

    global static String getAccessToken(String tokenUrl, String requestBody) {
        HttpExtension http = new HttpExtension();
        http.setMethod('POST');
        http.setEndPoint(tokenUrl); // Can be customized callout format.
        http.getRequest().setHeader('Content-type', 'application/x-www-form-urlencoded');
        http.getRequest().setBody(requestBody);
        HttpResponse resp = http.send();
        String responseBody = resp.getBody();
        integer statusCode = resp.getStatusCode();
        if (statusCode == 200) {
        	Map<String, Object> resultsBody = (Map<String, Object>) JSON.deserializeUntyped(responseBody);
            return String.valueOf(resultsBody.get('access_token'));
        } else {
            throw new AuthenticationException(statusCode, responseBody);
        }
    }
    
    global static String exchangeToken(JWTExchange__mdt config) {
        String JWTtoken = getJWTToken(config);
        String tokenUrl = config.TokenUrl__c;
        String body = 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=' + JWTtoken;
        return getAccessToken(tokenUrl, body);
    }

    /**
     * client_id and client_secret must be defined in NamedCredetials
     */
    global static String clientCredentialsToken(OAuth2Parameter__mdt config) {
        String tokenUrl = config.TokenUrl__c;
        if (String.isBlank(tokenUrl)) {
            return null;
        }
        String body = 'grant_type=client_credentials' +'&'+ 'client_id='+'{!$Credential.Username}' +'&'+ 'client_secret=' + '{!$Credential.Password}';
        if (String.isNotBlank(config.Scope__c)) {
            body += '&scope=' + config.Scope__c;
        }
        if (String.isNotBlank(config.Audience__c)) {
            body += '&audience=' + config.Audience__c;
        }
        return getAccessToken(tokenUrl, body);
    }

    @TestVisible
    private static String extractSchema(String endPoint) {
        Pattern pat = Pattern.compile(HTTP_EXT_SCHEMA_REGEX);
        Matcher match = pat.matcher(endPoint);
        if(match.find()) {
            return match.group(1); // schema
        }
        return null;
    }

    @TestVisible
    private static String extractPath(String endPoint) {
        Pattern pat = Pattern.compile(HTTP_EXT_SCHEMA_REGEX);
        Matcher match = pat.matcher(endPoint);
        if(match.find()) {
            return match.group(2); // schema
        }
        return '';
    }

    @TestVisible
    private static String extractConfigName(String endPoint) {
        Pattern pat = Pattern.compile(HTTP_EXT_SCHEMA_REGEX);
        Matcher match = pat.matcher(endPoint);
        if(match.find()) {
            return match.group(3); // config
        }
        return null;
    }
    
    global HttpResponse send() {
		String finalEndPoint = this.endPoint;
        String schema = extractSchema(endPoint);
        String configName = extractConfigName(endPoint);
        String path = extractPath(endPoint);
        if (schema == 'jwt') { // Start JWT Exchange
            JWTExchange__mdt config = getJWTConfig(configName);
            String accessToken = exchangeToken(config);
            if(String.isNotBlank(accessToken)) {
                request.setHeader('Authorization', 'Bearer ' + accessToken);
            }
            finalEndPoint = 'callout:' + path;
        } else  if (schema == 'client_credentials') {
            OAuth2Parameter__mdt config = getClientCredentialsConfig(configName);
            String accessToken = clientCredentialsToken(config);
            if(String.isNotBlank(accessToken)) {
                request.setHeader('Authorization', 'Bearer ' + accessToken);
            }
            finalEndPoint = 'callout:' + path;
        }
        request.setEndpoint(finalEndPoint);
        request.setMethod(method);
        Http http = new Http();
        return http.send(request);
    }

    global class JWTExchangeException extends Exception {}
    
    global class AuthenticationException extends Exception {
        private final Integer statusCode;
        private final String responseBody;

        global AuthenticationException(Integer statusCode, String responseBody) {
            this(statusCode + ': ' + responseBody);
            this.statusCode = statusCode;
            this.responseBody = responseBody;
        }

        global Integer getStatusCode() {
            return this.statusCode;
        }

        global String getResponseBody() {
            return this.responseBody;
        }
    }

}