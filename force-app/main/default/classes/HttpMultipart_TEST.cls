@IsTest
public class HttpMultipart_TEST {

    public class MyListener implements HttpMultipart.HttpListener {
        Integer statusCode = 0;
        String responseBody = '';
        public void onResponse(HttpResponse response) {
            statusCode = response.getStatusCode();
            responseBody = response.getBody();
        }
    }

    @IsTest
    static void sendRequest() {
        Test.setMock(HttpCalloutMock.class, new HttpMultipartMock(200, '{"code": "ok"}'));
        String endpoint = 'client_credentials:name_credentials/path/to/target';
        Map<String, String> messages = new Map<String,String>();
        messages.put('message', 'Hello world!');
        Map<String, HttpMultipart.FilePart> attachments = new Map<String, HttpMultipart.FilePart>();
        attachments.put('uploadFile', new HttpMultipart.FilePart('my-file.txt', 'data:@file/txt;base64,VGhpcyBpcyBhIHRleHQK'));
        
        Test.startTest();
        HttpMultipart.Callout callout = new HttpMultipart.Callout(endpoint, messages, attachments);
        MyListener listener = new MyListener();
        callout.setListener(listener).addHeader('x-token', 'my-secret-token');
        callout.execute(null);
        System.assertEquals(200, listener.statusCode);
        System.assertEquals('{"code": "ok"}', listener.responseBody);
        Test.stopTest();
    }
}