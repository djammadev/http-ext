@IsTest
global  class HttpMultipartMock implements HttpCalloutMock {
    Integer statusCode;
    String responseBody;
    global HttpMultipartMock(Integer statusCode, String responseBody) {
        this.statusCode = statusCode;
        this.responseBody = responseBody;
    }

    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        if (request.getEndpoint().contains('token')) {
            response.setStatusCode(200);
            response.setBody('{"expires_in": 3599, "access_token": "ejBAccessToken"}');
            return response;
        } 
        response.setStatusCode(statusCode);
        response.setBody(responseBody);
        return response;
    }
}